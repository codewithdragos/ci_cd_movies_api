openapi: 3.0.0
info:
    title: Movies API
    description: A fully fledged Movie and Actor api.
    version: 0.1.0
servers:
    - url: http://localhost:3000/api
      description: Local server used for development
    - url: ec2-18-195-117-132.eu-central-1.compute.amazonaws.com
      description: Staging server for the Movies API

security:
    - bearerAuth: [] # <-- use the same name here
paths:
    /movies/{movieId}:
        get:
            summary: Returns a movie by id.
            tags:
                - Movies
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                  description: UUID of the movie to get
            responses:
                '200': # status code
                    description: A movie object.
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Movie'
                '400': # status code
                    description: The movie id provided is not correct.
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/InvalidUUIDError'
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '404': # status code
                    description: The movie was not found.
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
        patch:
            summary: Updates a movie by id
            tags:
                - Movies
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                  description: UUID of the movie to update
            requestBody:
                description: The data of the movie to be created
                required: true
                content:
                    application/json:
                        schema:
                            $ref: '#/components/schemas/Movie'
            responses:
                '200': # status code
                    description: The updated movie object
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Movie'
                '404': # status code
                    description: The movie to update was not found.
                '400': # status code
                    description: The movie id provided is not correct or the data to update the movie is invalid.
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/ValidationError'
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
        delete:
            summary: Delete a movie by id
            tags:
                - Movies
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                  description: UUID of the movie to delete
            responses:
                '201': # status code
                    description: The movie was completelly deleted
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '404': # status code
                    description: The movie to delete was not found.
                '400': # status code
                    description: The movie id provided is not correct.
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/InvalidUUIDError'
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
    /movies:
        get:
            summary: Returns a list of movies.
            tags:
                - Movies
            responses:
                '200': # status code
                    description: A JSON array of movies
                    content:
                        application/json:
                            schema:
                                type: array
                                items:
                                    $ref: '#/components/schemas/Movie'
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
        post:
            summary: Creates a movie.
            tags:
                - Movies
            requestBody:
                description: The data of the movie to be created
                required: true
                content:
                    application/json:
                        schema:
                            type: object
                            properties:
                                title:
                                    type: string
                                    example: 'Princess Mononoke'
                                overview:
                                    type: string
                                    example: 'Ashitaka, a prince of the disappearing Emishi people, is cursed by a ...'
                                tagline:
                                    type: string
                                    example: 'The Fate Of The World Rests On The Courage Of One Warrior.'
                                runtime:
                                    type: string
                                    example: '02:05:00'
                                release_date:
                                    type: string
                                    example: '1988-04-16'
                                revenue:
                                    type: number
                                    example: 4500000
                                poster_path:
                                    type: string
                                    example: 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/rtGDOeG9LzoerkDGZF9dnVeLppL.jpg'
                            required:
                                - title
                                - release_date
            responses:
                '200': # status code
                    description: The movie resource created
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Movie'
                '400': # status code
                    description: The parameters provided are invalid
                    content:
                        application/json:
                            schema:
                                type: object
                                properties:
                                    message:
                                        type: string
                                        example: '"title" is required'
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
    /movies/{movieId}/reviews:
        get:
            summary: Retrieves a list of movie reviews.
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: UUID of the movie to get the reviews for
            tags:
                - Movie Reviews
            responses:
                '200': # status code
                    description: A JSON array of Movie Reviews
                    content:
                        application/json:
                            schema:
                                type: array
                                items:
                                    $ref: '#/components/schemas/MovieReview'
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
        post:
            summary: Creates a Movie Review.
            tags:
                - Movie Reviews
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: UUID of the movie for which the review is
            requestBody:
                description: The data of the movie review to be created
                required: true
                content:
                    application/json:
                        schema:
                            type: object
                            properties:
                                author_name:
                                    type: string
                                    example: 'Foo Bar'
                                content:
                                    type: string
                                    example: 'One of the best thriller movies of the 90s, I would highlly recommend.'
                                rating:
                                    type: integer
                                    example: 10
                            required:
                                - author_name
                                - rating
                                - content
            responses:
                '200': # status code
                    description: The movie review was created
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/MovieReview'
                '400': # status code
                    description: The parameters provided are invalid
                    content:
                        application/json:
                            schema:
                                type: object
                                properties:
                                    message:
                                        type: string
                                        example: '"author_name" is required'
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
    /movies/{movieId}/reviews/{reviewId}:
        get:
            summary: Returns a movie review using its id.
            tags:
                - Movie Reviews
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: UUID of the movie to get
                - in: path
                  name: reviewId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: The UUID of the review to get
            responses:
                '200': # status code
                    description: Returns the movire review
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/MovieReview'
                '400': # status code
                    description: The movie id or the review id are invalid
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '404': # status code
                    description: The movie or the review where not found
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
        patch:
            summary: Update a Movie Review by Id.
            tags:
                - Movie Reviews
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: UUID of the movie for which the review is
                - in: path
                  name: reviewId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: The UUID of the review to update
            responses:
                '200': # status code
                    description: Returns the movie review updated
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/MovieReview'
                '400': # status code
                    description: The update data is not valid
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '404': # status code
                    description: The movie or the review where not found
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'
        delete:
            summary: Delete a Movie Review.
            parameters:
                - in: path
                  name: movieId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: UUID of the movie and the movie review to delete
                - in: path
                  name: reviewId
                  required: true
                  schema:
                      type: string
                      example: '16e48282-21e3-4e6f-9bcc-a2f46f28b24d'
                  description: The UUID of the review to delete
            tags:
                - Movie Reviews
            responses:
                '201': # status code
                    description: The movie review was succesffully deleted
                '400': # status code
                    description: The movie id or the review id are not valid uuids.
                '401':
                    $ref: '#/components/responses/UnauthenticatedError'
                '403':
                    $ref: '#/components/responses/UnauthorizedError'
                '404': # status code
                    description: The movie or the review where not found
                '500': # status code
                    description: An error has occured
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/Error'

components:
    responses:
        UnauthenticatedError:
            description: Access token is missing or invalid
        UnauthorizedError:
            description: You don't have the permissions to access this endpoint
    securitySchemes:
        bearerAuth: # arbitrary name for the security scheme
            type: http
            scheme: bearer
            bearerFormat: JWT # optional, arbitrary value for documentation purposes
    schemas:
        ValidationError:
            type: object
            properties:
                message:
                    type: string
                    example: 'Property [x] on object [y] is not valid/missing.'
        Error:
            type: object
            properties:
                message:
                    type: string
                    example: 'An internal error has occurred'
        InvalidUUIDError:
            type: object
            properties:
                message:
                    type: string
                    example: 'The id provided for the resource is not valid.'
        Movie:
            type: object
            properties:
                id:
                    type: string
                    example: '48d9b2bc-3438-472a-9d83-c69c723d715c'
                title:
                    type: string
                    example: 'Princess Mononoke'
                overview:
                    type: string
                    example: 'Ashitaka, a prince of the disappearing Emishi people, is cursed by a ...'
                tagline:
                    type: string
                    example: 'The Fate Of The World Rests On The Courage Of One Warrior.'
                runtime:
                    type: string
                    example: '02:05:00'
                release_date:
                    type: string
                    example: '1988-04-16'
                revenue:
                    type: number
                    example: 4500000
                poster_path:
                    type: string
                    example: 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/rtGDOeG9LzoerkDGZF9dnVeLppL.jpg'
        MovieReview:
            type: object
            properties:
                id:
                    type: string
                    example: '48d9b2bc-3438-472a-9d83-c69c723d715c'
                author_name:
                    type: string
                    example: 'Foo Bar'
                content:
                    type: string
                    example: 'This was one of the best movie I have seen in the last years.'
                rating:
                    type: integer
                    example: 10
                movie_id:
                    type: string
                    example: 'eac34844-4ef1-4161-87a7-ccf4a691c624'
