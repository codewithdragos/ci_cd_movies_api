import Joi from 'joi'
import { DeepPartial } from 'typeorm'
import MovieController from '../movies/Movie.controller'
import { ControllerError } from '../types'
import MovieReviewController from './MovieReview.controller'
import MovieReview from './MovieReview.model'
import { MovieReviewCreateInputSchema } from './MovieReview.validators'
import { MovieReviewCreateDTO } from './MovieReviews.types'

export const movieReviewMutations = {
    movieReviewCreate: async (
        root: undefined,
        args: { input: MovieReviewCreateDTO }
    ): Promise<MovieReview | Joi.ValidationError | ControllerError> => {
        const result = MovieReviewCreateInputSchema.validate(args.input)
        try {
            if (result.error) {
                return result.error
            } else {
                console.log(args.input.movie_id, args.input)
                const newMovieReview =
                    await MovieReviewController.createMovieReview(
                        args.input.movie_id,
                        args.input
                    )
                console.log(newMovieReview, 'new movie review')
                return newMovieReview
            }
        } catch (error) {
            console.error(error)
            return {
                error_id: 'CREATE_MOVIE_REVIEW',
                message: 'An internal error has occurred.'
            }
        }
    }
}
