import { movieReviewMutations } from '../movieReviews/MovieReviews.resolver'
import MovieController from '../movies/Movie.controller'
import MovieResolver, { movieMutations } from '../movies/Movie.resolver'

export default {
    Query: {
        movieList: () => {
            return MovieController.getMovieList()
        }
    },
    Movie: {
        ...MovieResolver
    },
    Mutation: {
        ...movieMutations,
        ...movieReviewMutations
    }
}
