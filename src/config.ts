import { ServerConfig } from './types'

const config: ServerConfig = {
    port: 3000,
    db: {
        port: Number(process.env.DB_PORT) || 5432,
        host: process.env.DB_HOST || '',
        username: process.env.DB_USERNAME || '',
        password: process.env.DB_PASSWORD || ''
    }
}

export default config
