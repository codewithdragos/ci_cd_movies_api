# Setting up node ENV to production
export NODE_ENV=production

# installing dependencies
echo "Running npm install"
npm install

# clean up old app
echo "Killing the old app"
pm2 kill

# start app with pm2
echo "Running the app"
npm run prod