# Action Item: Deployment to `Virtual Servers` 🛰

In this `Action Item` you will:

-   ☁️ Get an `AWS Account` and learn the basics
-   🔋 Deploy an `API Server` using virtual servers from `AWS EC2`
-   💾 Use `AWS RDS` to set up a `cloud database`
-   🤖 Use `GitLab` to set up `an automatic release pipeline`

## Challenges:

1. [x] 💪[COMPETENT] Get an account on `GitLab`
2. [ ] 💪[COMPETENT] Get an `AWS` account
3. [ ] 💪[COMPETENT] Create a `GitLab` repository for the `API`
4. [ ] 💪[COMPETENT] Create a `cloud database` in `AWS RDS` for our production environment
5. [ ] 💪[COMPETENT] Create a `virtual server(instance)` in `EC2` and deploy to it manually
6. [ ] 🏋🏽‍♀️[PROFICIENT] Automatize the deployment by creating a pipeline in `Gitlab`
7. [ ] 🎁[BONUS] Add a `staging` instance and `database` and a new stage to the pipeline

> Make sure you set aside at least 2 hours of focused, uninterrupted work and give your best.

---

### <a name="app-structure"></a> App Architecture

![app-structure](examples/app_structure/app_structure.png)

<details closed>
<summary>CLICK ME! - 0. SETUP: Read me first!</summary>

### 0. 🚧 SETUP - READ ME FIRST! 🚧

You will need `Docker` to run both the tests and the app so make sure ==you have it running on your machine==.

1. Install dependencies:

```bash
    npm install
```

2. Install `Docker` and `Postman`

-   [Follow this link to install Docker](https://docs.docker.com/get-docker/)
-   [Follow this link to install POSTMAN](https://www.postman.com/downloads/)

3. Run the `Database` using `Docker`:

```bash
docker-compose up
```

4. Add a `.env` file(copy the example in `.env.example`):

```bash
npm touch .env
```

And start the server:

```bash
npm start
```

5. Useful guides:

-   **🖥️ [VIDEO Walkthrough - Application SETUP with Docker Compose](https://www.loom.com/share/2bacfa9a0a5947f585bc6934ff9eba99)**
-   **🖥️ [VIDEO Walkthrough - Using the Debugger for Node](https://www.loom.com/share/d9f0bf7f608643e987ac3de0378a074f)**
-   **🖥️ [VIDEO Walkthrough - Using POSTMAN to make API requests](https://www.loom.com/share/51426b696e544a5c98e66f62f4df4db4)**

💡 HINT: Use the `debugger` to find bugs quickly.
💡 HINT: Make sure the `tests` pass at all times(`TDD`).

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 1. Get an account on GitLab</summary>

## 1. Get an account on `GitLab`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 2. Get an AWS account</summary>

## 2. Get an account on `AWS`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 3. Create a GitLab repository for the API</summary>

## 3. Create a `GitLab` repository for the backend app

Check the [SUPPORT.md](SUPPORT.md) file in this repository for `step-by-step` instructions.

⚠️Before proceeding make sure your `API` source code is on `GitLab`.⚠️

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 4. Create a cloud database in AWS RDS for our production environment</summary>

### 4. Create a `Cloud Database` in `AWS RDS` for our `production` environment

In production, we will use a `Cloud Database` to host our `application`. To create the `database`:

#### 4.1 Create the Database in `RDS`

[Video Walkthrough - Create a Database in `RDS`](https://eu-central-1.console.aws.amazon.com/rds/home?region=eu-central-1#)

#### 4.2 Connect to the `database` from your `local environment`

[Video Walkthrough - Connect to the `RDS` database using PG Admin](https://eu-central-1.console.aws.amazon.com/rds/home?region=eu-central-1#)

#### 4.3 Add the database connection data to the `.env` file

```diff
# DB Config
+DB_PORT=5432
+DB_HOST=database-api-movie-production.cep4ujn358dp.eu-central-1.rds.amazonaws.com
+DB_USERNAME=root
+DB_PASSWORD=theSeniorDev
```

#### 4.4 Create a `Movie` and check it lives in the `cloud database`

##### 4.4.1 Get a `token` from `Auth0` for your application

##### 4.4.1 Make a post request using `Swagger`

##### 4.4.1 Check the tables in `pg-admin`

[Video Walkthrough - Create a record in the `production` database](https://eu-central-1.console.aws.amazon.com/rds/home?region=eu-central-1#)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 5. Create a Virtual Server(instance) in EC2 and, deploy to it manually</summary>

### 5. Create a virtual server in `EC2`, install `node.js`, and, deploy to it manually

#### 5.1 Create a `linux` virtual server(instance) on `EC2`:

[Video Guide on Creating an Instance](https://vimeo.com/689709236/93909bc394)

#### 5.2 Connect to the virtual server on `EC2` with `ssh`, install `node.js and upload the files``

You can follow the [official documentation here](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html) or watch the video below.

[Video Guide on installing Node.js](https://vimeo.com/689708631/5b28cc224a)

#### 5.3 Upload the code with `scp` and deploy it manually

You can now use `scp` to upload your code to the virtual instance. Replace `AWS_KEY_PATH` with the local path to the `.pem` file on your computer and `INSTANCE_IP` with the public IP of your instance and run the commands below:

```bash
# set right permissions on your key
chmod 400 AWS_KEY_PATH
# copy files to the ec2 instance
scp -i movie-api-instance-prod-key.pem -r build ec2-user@52.59.194.108:/home/ec2-user/build
scp -i movie-api-instance-prod-key.pem -r package.json ec2-user@52.59.194.108:/home/ec2-user/package.json

scp -i AWS_KEY_PATH package.json ec2-user@INSTANCE_IP:/home/ec2-user/build/package.json
```

If you get stuck [here is a Quick Video Guide](https://vimeo.com/689708785/8ba15206e6)

#### 5.4 Create a `.env` file in the `AWS` instance

Run the `nano` editor in the `terminal` of the instance, inside the build folder:

```bash
nano .env
```

And add inside the content of your local `.env` file.

#### 5.5 Install `pm2` on the `instance` and run your app:

_Note: `PM2` runs `node.js` as a background process, so the app is live, even when we close our `ssh` session. We will get deeper into how it works later on._

```bash
npm install pm2 -g
# in the ./build folder on the virtual machine -- instance
pm2 start index.js -i max
```

#### 5.6 Check the application is now live on the `EC2` instance:

Go to [http://YOUR_INSTANCE_PUBLIC_IP/api-docs/](http://INSTANCE_IP/api-docs/)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 6. Automatize the deployment by creating a pipeline in Gitlab</summary>

### 6. Automatize the `deployment` by creating a `pipeline` in `Gitlab`

We have been able to `deploy` manually but is very time-consuming and error-prone.

Let's automate the deployment using `Gitlab`.

#### 6.1. In `Gitlab` go to pipelines:

![Create Pipeline Step 1](examples/add_pipeline_step_one.png)

#### 6.2. Click **Create New Pipeline** && remove the default code:

![Create Pipeline Step 2](examples/add_pipeline_step_two.png)

#### 6.3. Add the following code instead:

```yaml
image: node:16
stages: ['build', 'test', 'deploy']

# WARNING
# This pipeline needs the following variables set up to work:
# INSTANCE_IP =  // the public IP of the AWS instance
# SECRET_KEY = // the secret key to connect to the AWS instance (.pem) file
# ENV_FILE = // the .env file for production

cache:
    policy: pull-push
    untracked: false
    when: on_success
    paths:
        - 'node_modules'

# the build job
build_job:
    stage: build
    script:
        - npm install
        - npm run build
    artifacts:
        paths:
            - build
        expire_in: 1 week

test_job:
    stage: test
    image: node:16

    services: # we need docker in order to be able to run the tests
        - name: docker:20.10.1-dind
          command: ['--tls=false', '--host=tcp://0.0.0.0:2376']

    variables:
        DOCKER_HOST: 'tcp://docker:2375'
        DOCKER_TLS_CERTDIR: ''
        DOCKER_DRIVER: 'overlay2'

    script:
        - export CI=true
        - npm ci
        - npm test
    needs:
        - build_job
    dependencies:
        - build_job

deploy_job:
    stage: deploy
    only:
        - main
    before_script: # prepare the pipeline runner for deploy by installing ssh
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
        - apt-get update -y
        - apt-get -y install rsync
    script:
        - chmod 400 $SECRET_KEY
        # clean up the ec32 instance
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'rm -rf /home/ec2-user/api'
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'mkdir /home/ec2-user/api'

        # copy files to the ec2 instance
        - scp -i $SECRET_KEY -r build ec2-user@$INSTANCE_IP:/home/ec2-user/api
        - scp -i $SECRET_KEY package.json ec2-user@$INSTANCE_IP:/home/ec2-user/api/package.json
        - scp -i $SECRET_KEY deploy.sh ec2-user@$INSTANCE_IP:/home/ec2-user/api/deploy.sh

        # copy .env to the ec2 instance
        - scp -i $SECRET_KEY $ENV_FILE ec2-user@$INSTANCE_IP:/home/ec2-user/api/.env.production

        # run the deploy script
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'cd /home/ec2-user/api && bash deploy.sh'
    needs:
        - job: build_job
          artifacts: true
        - job: test_job
          artifacts: false
    dependencies:
        - build_job
        - test_job
```

---

#### 6.4 Add a `deploy.sh` script to be executed by Gitlab in the deployment job:

Create the `deploy.sh` file in the root folder of the repository:

```bash
touch deploy.sh
```

An add to it the following code:

```bash
# clean up old app
pm2 kill

# Setting up node ENV to production
export NODE_ENV=production

# installing dependencies
echo "Running npm install"
npm install

# start app with pm2
echo "Running the app"
npm run prod
```

#### 6.5 Add the production and monitor script to `package.json`:

```diff
  "scripts": {
    "build": "rm -rf build && tsc && npm run copy-files",
    "test": "jest",
+   "prod": "NODE_ENV=production & pm2 start build/index.js  --name 'api' -f",
    "start": "NODE_ENV=development nodemon src/index.ts",
    "copy-files": "copyfiles -u 1 src/**/*.yaml build/ & copyfiles -u 1 src/**/*.graphql build/",
    "orm:migrate-gen": "typeorm-ts-node-commonjs migration:generate ./src/database/migrations/movie-api -d ./src/database/ApiDataSource.ts ",
    "orm:migrate-run": "typeorm-ts-node-commonjs -d ./src/database/ApiDataSource.ts migration:run"
  },
```

#### 6.6 Add the missing variables the `ci-cd` file needs: INSTANCE_IP, SECRET_KEY, and ENV_FILE

##### 6.6.1 Add `INSTANCE_IP` to the `Gitlab` variables

![Add INSTANCE_IP](examples/add_instance_ip_variable.png)

##### 6.6.2 Add `SECRET_KEY` to the `Gitlab` variables

![Add SECRET_KE](examples/add_secret_key_variable.png)

##### 6.6.3 Add `ENV_FILE` to the `Gitlab` variables

![Add ENV_FILE](examples/add_env_file_variable.png)

##### 6.6.4 Push some code or run the `pipeline` manually

```bash
git push
```

#### 6.7 If the pipeline does not run automatically, run the pipeline manually:

![Run the pipeline](examples/add_pipeline_step_sixteen.png)

#### 6.8 When the `pipeline` is finished you should be live:

![API is live](examples/api_in_the_cloud.png)

**Congratulations!!! You can now run applications in the `cloud` and deploy them automatically!**

</details>

---

<details closed>
<summary>CLICK ME! - 7. SOLUTION: Add a staging instance and database and update the pipeline</summary>

### 7. Add a `staging instance` and a `staging database` and update the pipeline

It would also be great if we could add a `rollback` mechanism or a `staging` environment.

##### 7.1. Add a new `staging instance` in AWS following the same process we did for the production instance

> NOTE: you can use the prefix `-staging` to make sure you don't confuse both instances

##### 7.2. Add a new `staging database` in RED following the same process we did for the production database

> NOTE: you can use the prefix `-staging` to make sure you don't confuse both databases

##### 7.3. Add a `staging` step to your `.gitlab-ci.yml` file:

##### 7.4. Update the `environment` variables to match the bucket names:

Modify the bucket name:

```bash
AWS_BUCKET_NAME --> AWS_PRODUCTION_BUCKET_NAME
```

And add the `staging` bucket name:

```bash
AWS_STAGING_BUCKET_NAME --> The name of the bucket you created for staging
```

##### 7.5. Test your multistage deployment:

###### 7.5.1 Create a new `staging` branch and push some changes

You can just change some text in the app or a CSS property.

###### 7.5.2 Push to `staging` and observe the pipeline running but only the staging deployment being executed

###### 7.5.3 Create a Pull Request from `staging` to `main`

###### 7.5.4 Merge the Pull Request and observe the `deployment` to the `production` bucket

</details>

### Getting Help

If you have issues with the Action Item, you can ask for help in the [Community](https://community.theseniordev.com/) or in the [Weekly Q&As](https://calendar.google.com/calendar/u/0?cid=Y19kbGVoajU1Z2prNXZmYmdoYmxtdDRvN3JyNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t).

### Made with :orange_heart: in Berlin by @TheSeniorDev
